# Csharp Console  Beginners Premium Puzzle 1
## Thank you, for contributing to the channel! 
### Your money helps support me and allows me to keep making these learning puzzles.

### View the Course Video: 


## Puzzles
In Program.cs, I have commented out some starting code for these puzzles. <br />
Write your code in these sections and uncomment them. <br />
I have provided some examples of similar functions to what each puzzle is asking for. Learn to read the code and modify it. <br />

At the very bottom, I have listed the functions. Comment and uncomment them, to run them one at a time. <br />

```
// Run the puzzles

    ExampleA();
    //PuzzleA();

    ExampleB();
    //PuzzleB();

    ExampleC();
    //PuzzleC();
```

### A -  Your favourite food
Write a program that asks "What is your favourite food?: " <br />
Get the user's input <br />
Use String.Concat() to print "mmmm {favFood} sounds tasty!" <br />


### B - Upper Case
Write a program that asks the user "Please enter a word: " <br />
Convert that input to all upper case <br />
Using string interpolation, not string concatenation, return "What do you mean? {WORD} !!!" <br />


### C – Control Characters
Escape all the control characters in the examples below, so that the program runs <br />


# Good Luck!