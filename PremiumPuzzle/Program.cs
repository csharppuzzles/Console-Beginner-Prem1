﻿
static void ExampleA()
{
    Console.WriteLine("\nExample A");
    Console.WriteLine("~~~~~~~~~");

    Console.WriteLine("\nEnter your name: ");
    string? name = Console.ReadLine();

    // String concatination using String.Concat()
    Console.WriteLine(string.Concat("Your name is ", name));

}


// Puzzle A
// Write a program that asks "What is your favourite food?: "
// Get the user's input
// Use String.Concat() to print "mmmm {favFood} sounds tasty!"

/*
static void PuzzleA()
{
    Console.WriteLine("\nPuzzle A");
    Console.WriteLine("~~~~~~~~~");
}
*/


static void ExampleB()
{
    Console.WriteLine("\nExample B");
    Console.WriteLine("~~~~~~~~~");

    Console.WriteLine("Do you like dogs?: ");
    string? input = Console.ReadLine();

    Console.WriteLine("What ??? " + input + " ...really?");
}


// Puzzle B
// Write a program that asks the user "Please enter a word: "
// Convert that input to all upper case
// Using string interpolation, not string concatenation, return "What do you mean? {WORD} !!!"

/*
static void PuzzleB()
{
    Console.WriteLine("\nPuzzle B");
    Console.WriteLine("~~~~~~~~~");

}
*/

static void ExampleC()
{
    Console.WriteLine("\nExample C");
    Console.WriteLine("~~~~~~~~~");

    // Normally, you have to escape control characters like backslash \\ or quotes \" \" or \n new line
    Console.WriteLine("What's the \"plan\" again?");

    Console.WriteLine("This is a backslash \\");

    // '@' means verbatim. Read the string literally and don't interpret control characters like backslash
    Console.WriteLine(@"This is a backslash \");

}

/*
// Puzzle C
// Escape all the control characters in the examples below, so that the program runs
static void PuzzleC()
{
    Console.WriteLine("\nPuzzle C");
    Console.WriteLine("~~~~~~~~~");

    string example1 = "Yes, you are "Special"";
    Console.WriteLine(example1);

    string example2 = "n Line 1 n Line 2 n Line 3";
    Console.WriteLine(example2);

    string example3 = "C:\Documents\MyFolder\MyFile.txt";
    Console.WriteLine(example3);
}
*/



// Run the puzzles

ExampleA();
//PuzzleA();

ExampleB();
//PuzzleB();

ExampleC();
//PuzzleC();



Console.WriteLine("\n Press enter to exit the program...");
Console.ReadLine();                                         // Keeps the console app window open until you hit enter